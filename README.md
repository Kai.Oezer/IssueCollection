# IssueCollection

A small Swift module for collecting runtime issues, similar to a logging utility.
The collected issues are not processed, however. They are just stored for accessing them later.

## Usage

``Issue`` items can be categorized through their string based ``domain``, and
integer based ``code`` properties.
_Issue_ allows storing item-specific data in its ``metadata`` dictionary property.

Customize issue types by creating extensions of ``IssueDomain``, ``IssueCode``, and ``IssueMetadataKey``
and adding static constants that fit your use case.
