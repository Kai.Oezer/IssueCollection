// Copyright 2024 Kai Oezer

import Foundation

public struct IdentifiableIssue : Identifiable, Codable
{
	public let issue : Issue
	public let id : UUID
	
	public init(_ issue : Issue)
	{
		self.issue = issue
		id = UUID()
	}
	
	private enum CodingKeys : String, CodingKey
	{
		case issue
		case identifier
	}

	public init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: CodingKeys.self)
		issue = try container.decode(Issue.self, forKey: .issue)
		id = try container.decode(UUID.self, forKey: .identifier)
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(issue, forKey: .issue)
		try container.encode(id, forKey: .identifier)
	}
}

extension IdentifiableIssue : Equatable
{
	public static func == (lhs: IdentifiableIssue, rhs: IdentifiableIssue) -> Bool
	{
		(lhs.issue == rhs.issue) && (lhs.id == rhs.id)
	}
}

extension IdentifiableIssue : Comparable
{
	public static func < (lhs: IdentifiableIssue, rhs: IdentifiableIssue) -> Bool
	{
		(lhs.issue == rhs.issue) ? (lhs.id.uuidString < rhs.id.uuidString) : (lhs.issue < rhs.issue)
	}
}
