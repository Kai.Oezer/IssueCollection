// Copyright 2022-2024 Kai Oezer

import Foundation
#if canImport(Combine)
import Combine
#endif

/// Provides convenience functions for operating on collected issues
public struct IssueCollection : Codable, Sendable
{
	public var issues : [Issue]

	public init(issues : [Issue] = [])
	{
		self.issues = issues
	}

	public mutating func append(_ issue : Issue)
	{
		issues.append(issue)
	}

	public mutating func append(_ collection : IssueCollection)
	{
		issues.append(contentsOf: collection.issues)
	}

	public static func + (left : IssueCollection, right : IssueCollection) -> IssueCollection
	{
		return IssueCollection(issues: left.issues + right.issues)
	}

	public var count : Int { issues.count }

	public func issues(for domain : IssueDomain) -> [Issue]
	{
		issues.filter{ $0.domain == domain }
	}

	public var sorted : [Issue] { issues.sorted() }

	public var shortDescriptionLines : [String]
	{
		issues.map{ "[\($0.domain)][\($0.code.code)]" + ($0.message != nil ? "  \($0.message!)" : "") }
	}

	public mutating func clear()
	{
		issues.removeAll()
	}

	enum CodingKeys : String, CodingKey
	{
		case issues
	}

	public init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: CodingKeys.self)
		issues = try container.decode([Issue].self, forKey: .issues)
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(issues, forKey: .issues)
	}
}

extension IssueCollection : CustomStringConvertible
{
	public var description : String
	{
		issues.map{ $0.description }.joined(separator: "\n")
	}
}

extension IssueCollection : Equatable
{
	public static func == (lhs: IssueCollection, rhs: IssueCollection) -> Bool
	{
		lhs.issues == rhs.issues
	}
}

extension IssueCollection : Collection
{
	public var startIndex : Array<Issue>.Index
	{
		issues.startIndex
	}

	public var endIndex : Array<Issue>.Index
	{
		issues.endIndex
	}

	public subscript(index : Array<Issue>.Index) -> Issue
	{
		issues[index]
	}

	public func index(after i : Array<Issue>.Index) -> Array<Issue>.Index
	{
		issues.index(after: i)
	}
}

extension IssueCollection : RandomAccessCollection
{
	public func index(before i : Array<Issue>.Index) -> Array<Issue>.Index
	{
		issues.index(before: i)
	}
}

#if canImport(Combine)
public class ObservableIssueCollection : ObservableObject
{
	@Published var issues = IssueCollection()

	public init() {}
}
#endif
