// Copyright 2022-2024 Kai Oezer

import Foundation

public struct IssueDomain : Sendable, Codable
{
	let domain : String

	public init(_ domain : String)
	{
		self.domain = domain
	}
}

extension IssueDomain : ExpressibleByStringLiteral
{
	public init(stringLiteral value: StringLiteralType)
	{
		self.domain = value
	}
}

extension IssueDomain : Equatable
{
	public static func == (lhs : IssueDomain, rhs : IssueDomain) -> Bool
	{
		lhs.domain == rhs.domain
	}
}

extension IssueDomain : Comparable
{
	public static func < (lhs : IssueDomain, rhs : IssueDomain) -> Bool
	{
		lhs.domain < rhs.domain
	}
}

extension IssueDomain : Hashable
{

}

extension IssueDomain : CustomStringConvertible
{
	public var description : String { domain }
}
