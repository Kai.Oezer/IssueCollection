// Copyright 2022-2024 Kai Oezer

import Foundation

public struct Issue : Sendable, Codable
{
	public typealias Metadata = [IssueMetadataKey : String]

	public let domain : IssueDomain
	public let code : IssueCode
	public let metadata : Metadata
	public var message : String? { metadata[.message] }

	public init(domain : IssueDomain, code : IssueCode, metadata : Metadata)
	{
		self.domain = domain
		self.code = code
		self.metadata = metadata
	}

	public init(domain : IssueDomain, code : IssueCode, message : String? = nil)
	{
		self.init(domain: domain, code: code, metadata: message != nil ? [.message : message!] : [:])
	}

	public subscript(key : IssueMetadataKey) -> String?
	{
		metadata[key]
	}

	enum CodingKeys : String, CodingKey
	{
		case domain
		case code
		case metadata
	}

	public init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: CodingKeys.self)
		domain = try IssueDomain(container.decode(String.self, forKey: .domain))
		code = try IssueCode(container.decode(Int.self, forKey: .code))
		metadata = try container.decode(Metadata.self, forKey: .metadata)
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(domain.domain, forKey: .domain)
    try container.encode(code.code, forKey: .code)
		try container.encode(metadata, forKey: .metadata)
	}
}

extension Issue : Equatable
{
	public static func == (lhs : Issue, rhs : Issue) -> Bool
	{
		(lhs.domain == rhs.domain) && (lhs.code == rhs.code) && (lhs.metadata == rhs.metadata)
	}
}

extension Issue : Comparable
{
	public static func < (lhs: Issue, rhs: Issue) -> Bool
	{
		if lhs.domain != rhs.domain {
			return lhs.domain < rhs.domain
		}
		if lhs.code != rhs.code {
			return lhs.code < rhs.code
		}
		let concatKeys : (Metadata)->String = { metadata in
			return metadata.keys.sorted().reduce(""){ $0 + $1.value }
		}
		let lhsKeys = concatKeys(lhs.metadata)
		let rhsKeys = concatKeys(rhs.metadata)
		if lhsKeys != rhsKeys {
			return lhsKeys < rhsKeys
		}
		let concatValues : (Metadata)->String = { metadata in
			return metadata.sorted{ $0.key < $1.key }.reduce(""){ $0 + $1.value }
		}
		let lhsValues = concatValues(lhs.metadata)
		let rhsValues = concatValues(rhs.metadata)
		return lhsValues < rhsValues
	}
}

extension Issue : CustomStringConvertible
{
	public var description : String
	{
		var lines = [String]()
		if let message {
			lines.append("[\(domain)][\(code)]  \(message)")
		} else {
			lines.append("[\(domain)][\(code)]")
		}
		for key in metadata.keys.sorted() {
			if key != .message {
				lines.append("  􀄵 \(key) : \(metadata[key]!)")
			}
		}
		return lines.joined(separator: "\n")
	}
}

extension Issue : Hashable
{
	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(domain)
		hasher.combine(code)
		hasher.combine(metadata)
	}
}

extension Issue : Identifiable
{
	public var id : Issue { self }
}
