// Copyright 2023-2024 Kai Oezer

import Foundation

/// Extend this type to add new metadata keys.
public struct IssueMetadataKey : Sendable, Codable, Hashable, ExpressibleByStringLiteral
{
	public let value : String

	public init(_ value : String)
	{
		self.value = value
	}

	public init(stringLiteral value: StringLiteralType)
	{
		self.init(value)
	}

	public static let message = IssueMetadataKey("message")
}

extension IssueMetadataKey : Comparable
{
	public static func < (lhs: IssueMetadataKey, rhs: IssueMetadataKey) -> Bool
	{
		lhs.value < rhs.value
	}
}

extension IssueMetadataKey : Equatable
{
	public static func == (lhs: IssueMetadataKey, rhs: IssueMetadataKey) -> Bool
	{
		lhs.value == rhs.value
	}
}

extension IssueMetadataKey : CustomStringConvertible
{
	public var description : String { value }
}
