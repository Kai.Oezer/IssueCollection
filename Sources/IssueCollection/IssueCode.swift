// Copyright 2022-2024 Kai Oezer

import Foundation

public struct IssueCode : Sendable, Codable
{
	public let code : Int

	public init(_ code : Int)
	{
		self.code = code
	}
}

extension IssueCode : ExpressibleByIntegerLiteral
{
	public init(integerLiteral value: Int)
	{
		self.code = value
	}
}

extension IssueCode : Equatable
{
	public static func == (lhs : IssueCode, rhs : IssueCode) -> Bool
	{
		lhs.code == rhs.code
	}
}

extension IssueCode : Comparable
{
	public static func < (lhs : IssueCode, rhs : IssueCode) -> Bool
	{
		lhs.code < rhs.code
	}
}

extension IssueCode : Hashable
{
}

extension IssueCode : CustomStringConvertible
{
	public var description : String { "\(code)" }
}
