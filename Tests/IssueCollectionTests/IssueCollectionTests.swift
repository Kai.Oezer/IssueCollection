// Copyright 2022-2024 Kai Oezer

import Foundation
import Testing
@testable import IssueCollection

@Test("creating issues with text message")
func creatingIssuesWithTextMessage() throws
{
	let message = "this is a test issue"
	var collection = IssueCollection()
	collection.append(Issue(domain: .domain1, code: .code1, message: message))
	#expect(collection.issues.count == 1)
	#expect(collection.issues.first?.message == message)
}

@Test("create issues with metadata")
func creatingIssuesWithMetadata() throws
{
	let dataItem = 5
	var collection = IssueCollection()
	collection.append(Issue(domain: .domain1, code: .code1, metadata: [.testData : String(dataItem)]))
	#expect(collection.issues.count == 1)
	let testDataString = try #require(collection.issues.first?[.testData])
	#expect(Int(testDataString)! == dataItem)
}

@Test("domain filtering")
func domainFiltering()
{
	var collection = IssueCollection()
	collection.append(Issue(domain: .domain1,  code: .code1, message: "issue in domain 1"))
	collection.append(Issue(domain: .domain1,  code: .code1, message: "issue in domain 1"))
	collection.append(Issue(domain: .domain2, code: .code1, message: "issue in domain 2"))
	collection.append(Issue(domain: .domain2, code: .code1, message: "issue in domain 2"))
	collection.append(Issue(domain: .domain2, code: .code1, message: "issue in domain 2"))
	collection.append(Issue(domain: .domain3, code: .code1, message: "issue in domain 3"))
	collection.append(Issue(domain: .domain3, code: .code1, message: "issue in domain 3"))
	collection.append(Issue(domain: .domain3, code: .code1, message: "issue in domain 3"))
	collection.append(Issue(domain: .domain3, code: .code1, message: "issue in domain 3"))
	#expect(collection.issues(for: .domain1).count == 2)
	#expect(collection.issues(for: .domain2).count == 3)
	#expect(collection.issues(for: .domain3).count == 4)
	#expect(collection.issues(for: .domain3).reduce(true){ $0 && $1.domain == .domain3 })
}

@Test
func clearing()
{
	var collection = IssueCollection()
	collection.append(Issue(domain: .domain1, code: .code1, message: "issue 1"))
	collection.append(Issue(domain: .domain1, code: .code1, message: "issue 2"))
	collection.append(Issue(domain: .domain1, code: .code1, message: "issue 3"))
	collection.append(Issue(domain: .domain1, code: .code1, message: "issue 4"))
	collection.append(Issue(domain: .domain1, code: .code1, message: "issue 5"))
	collection.append(Issue(domain: .domain1, code: .code1, message: "issue 6"))
	#expect(collection.count == 6)
	collection.clear()
	#expect(collection.count == 0)
}

@Test("description lines")
func descriptionLines()
{
	var collection = IssueCollection()
	collection.append(Issue(domain: .domain1, code: .code1, message: "issue 1"))
	collection.append(Issue(domain: .domain1, code: .code1, metadata: ["name":"John", "role":"driver"]))
	collection.append(Issue(domain: .domain1, code: .code1, message: "issue 3"))
	collection.append(Issue(domain: .domain1, code: .code1, message: "issue 4"))
	#expect(collection.shortDescriptionLines == [
		"[test_domain][1]  issue 1",
		"[test_domain][1]",
		"[test_domain][1]  issue 3",
		"[test_domain][1]  issue 4"
	])
	#expect(collection.description ==
		"""
		[test_domain][1]  issue 1
		[test_domain][1]
		  􀄵 name : John
		  􀄵 role : driver
		[test_domain][1]  issue 3
		[test_domain][1]  issue 4
		"""
	)
}

@Test("appending collection")
func appendingCollection()
{
	var collection1 = IssueCollection()
	collection1.append(Issue(domain: .domain1, code: .code1, message: "issue 1.1"))
	collection1.append(Issue(domain: .domain1, code: .code1))
	collection1.append(Issue(domain: .domain1, code: .code1, message: "issue 1.3"))
	collection1.append(Issue(domain: .domain1, code: .code1, message: "issue 1.4"))
	var collection2 = IssueCollection()
	collection2.append(Issue(domain: .domain2, code: .code1, message: "issue 2.1"))
	collection2.append(Issue(domain: .domain2, code: .code1))
	collection2.append(Issue(domain: .domain2, code: .code1, message: "issue 2.3"))
	collection2.append(Issue(domain: .domain2, code: .code1, message: "issue 2.4"))
	let oldCount = collection1.count
	collection1.append(collection1)
	#expect(collection1.count == 2 * oldCount)
	collection1.append(collection2)
	#expect(collection1.count == 2 * oldCount + collection2.count)
	#expect(collection1.issues(for: .domain2).count == collection2.count)
	collection1.append(collection2)
	#expect(collection1.issues(for: .domain2).count == 2 * collection2.count)
}

@Test
func encoding() throws
{
	var collection = IssueCollection()
	collection.append(Issue(domain: .domain1, code: .code1, message: "issue 1"))
	collection.append(Issue(domain: .domain1, code: .code1))
	let jsonEncoder = JSONEncoder()
	jsonEncoder.outputFormatting = [.prettyPrinted, .sortedKeys]
	let encodedIssuesData = try jsonEncoder.encode(collection)
	let encodedIssues = try #require(String(data: encodedIssuesData, encoding: .utf8))
	let expectedOutput =
"""
{
  "issues" : [
    {
      "code" : 1,
      "domain" : "test_domain",
      "metadata" : [
        {
          "value" : "message"
        },
        "issue 1"
      ]
    },
    {
      "code" : 1,
      "domain" : "test_domain",
      "metadata" : [

      ]
    }
  ]
}
"""
	#expect(encodedIssues == expectedOutput)
	let decodedCollection = try JSONDecoder().decode(IssueCollection.self, from: encodedIssuesData)
	#expect(decodedCollection == collection)
}

@Test("checking equality")
func checkingEquality()
{
	let a = Issue(domain: .domain1, code: 100)
	let b = Issue(domain: .domain2, code: 100)
	let c = Issue(domain: .domain1, code: 5)
	let d = Issue(domain: .domain1, code: 100)
	let e = Issue(domain: .domain1, code: 100, metadata: [.message : "one"])
	let f = Issue(domain: .domain1, code: 100, metadata: [.message : "two"])
	let g = Issue(domain: .domain1, code: 100, metadata: [.message : "one"])
	let h = Issue(domain: .domain1, code: 100, metadata: [.message : "one", .testData : "data"])

	#expect(a != b)
	#expect(a == d)
	#expect(a != c)
	#expect(a != e)
	#expect(e != f)
	#expect(e == g)
	#expect(e != h)
}

@Test("comparing issues")
func comparingIssues()
{
	let a = Issue(domain: .domain1, code: .code1, message: "bla")
	let b = Issue(domain: .domain1, code: .code1, message: "bla bla")
	let c = Issue(domain: .domain2, code: .code1, message: "bla bla")
	let d = Issue(domain: .domain2, code: .code1)
	let e = Issue(domain: .domain2, code: .code2)
	let f = Issue(domain: .domain1, code: .code1)
	#expect(a < b, "bla < bla bla")
	#expect(a > c, "test_domain > another_test_domain")
	#expect(b > c, "test_domain > another_test_domain")
	#expect(a > d, "test_domain > another_test_domain")
	#expect(d < e)
	#expect(a > f)
}

@Test("identifiable issues")
func identifiableIssues()
{
	let a = Issue(domain: .domain1, code: .code1, message: "issue 1")
	let ia1 = IdentifiableIssue(a)
	let ia2 = IdentifiableIssue(a)
	#expect(ia1 != ia2)
}

extension IssueDomain
{
	static let domain1 : IssueDomain = "test_domain"
	static let domain2 : IssueDomain = "another_test_domain"
	static let domain3 : IssueDomain = "yet another test domain"
}

extension IssueCode
{
	static let code1 : IssueCode = 1
	static let code2 : IssueCode = 10
}

extension IssueMetadataKey
{
	static let testData : IssueMetadataKey = "test_data"
}
