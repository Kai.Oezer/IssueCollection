// swift-tools-version:6.0

import PackageDescription

let package = Package(
	name: "IssueCollection",
	platforms: [.iOS(.v13), .macOS(.v10_15), .tvOS(.v13), .watchOS(.v6)],
	products: [
		.library(
			name: "IssueCollection",
			targets: ["IssueCollection"])
	],
	dependencies: [
	],
	targets: [
		.target(
			name: "IssueCollection",
			dependencies: []),
		.testTarget(
			name: "IssueCollectionTests",
			dependencies: ["IssueCollection"])
	]
)
